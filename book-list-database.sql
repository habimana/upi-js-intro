CREATE TABLE books (id INTEGER PRIMARY KEY, name text, rating integer );

INSERT INTO books VALUES (1,"Poor dads rich dads",4);
INSERT INTO books VALUES (2, "Journal",2);
INSERT INTO books VALUES (3,"Bookings",5);
